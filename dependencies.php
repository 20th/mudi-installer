<?php

/**
 * @file
 */

namespace Mudi {
    class Exception extends \RuntimeException
    {
    }


    class Environment implements \ArrayAccess
    {

        const BAD_DIR = 'Directory %s does not exist.';

        const BAD_PERM = 'Directory %s is not readable. Please check filesystem permissions.';


        public function __construct()
        {
            $this->resetDir();
        }

        public function __invoke($path)
        {
            $path = $this->in($path);

            $commands = func_get_args();
            array_shift($commands);

            foreach ($commands as $command) {
                $path->run($command);
            }

            return $path;
        }

        public function offsetExists($path)
        {
            return $this->changeDir($path) ? true : false;
        }

        public function offsetGet($path)
        {
            return $this->in($path);
        }

        public function offsetSet($path, $command)
        {
            $path = $this->in($path);
            $path->run($command);

            return $path;
        }

        public function offsetUnset($path)
        {
            throw new Exception();
        }

        public function in($path)
        {
            return new Path($this->changeDir($path));
        }

        private function resetDir()
        {
            chdir(realpath(__DIR__));
        }

        private function changeDir($path)
        {
            $this->resetDir();

            $changed = array();
            foreach (array_filter(explode('/', $path)) as $dir) {
                $changed[] = $dir;

                if (!is_dir($dir)) {
                    throw new Exception(sprintf(self::BAD_DIR, implode('/', $changed)));
                } elseif (!is_readable($dir)) {
                    throw new Exception(sprintf(self::BAD_PERM, implode('/', $changed)));
                }

                chdir($dir);
            }

            return implode('/', $changed);
        }

    }


    class Path implements \ArrayAccess
    {

        const FAILED = '%s execution failed.';


        private $path;


        public function __construct($path)
        {
            $this->path = $path;
        }

        public function offsetExists($command)
        {
            return true;
        }

        public function offsetGet($command)
        {
            return $this->run($command);
        }

        public function offsetSet($command, $value)
        {
            throw new Exception();
        }

        public function offsetUnset($command)
        {
            throw new Exception();
        }

        public function run($command)
        {
            if (is_array($command)) {
                foreach ($command as $i) {
                    $this->run($i);
                }

                return $this;
            }

            $failed = '';
            $returnVal = 0;

            if ($command instanceof Task) {
                $command->before();
            }

            if ($command != '') {
                echo sprintf("# Running %s in %s/...\n", $command, $this->path);
                passthru($command, $returnVal);
                echo "\n";
            }

            if ($returnVal != 0) {
                throw new Exception(sprintf(self::FAILED, $command));
            }

            if ($command instanceof Task) {
                $command->after();
            }

            return $this;
        }

        public function exec($command)
        {
            return $this->run($command);
        }

        public function sh($command)
        {
            return $this->run($command);
        }

        public function _($command)
        {
            return $this->run($command);
        }

    }


    class Task
    {

        private $beforeCallback;

        private $command;

        private $afterCallback;


        public function __construct($before = null, $command = null, $after = null)
        {
            $this->beforeCallback = $before;
            $this->command = $command;
            $this->afterCallback = $after;
        }

        public function before()
        {
            if ($this->beforeCallback) {
                $callback = $this->beforeCallback;
                $callback();
            }
        }

        public function __toString()
        {
            return strval($this->command);
        }

        public function after()
        {
            if ($this->afterCallback) {
                $callback = $this->afterCallback;
                $callback();
            }
        }

    }


    abstract class Module
    {

        const SYSTEM = 'system';

        const LOCAL = 'local';


        protected $scope;


        public static function name()
        {
            return null;
        }

        public static function system()
        {
            return null;
        }

        public static function local()
        {
            return null;
        }

        public static function defaultScope()
        {
            return null;
        }

        public static function defaultAction()
        {
            return null;
        }

        protected static function uniqueKey()
        {
            static $counter = 0;

            return 'c' . $counter++;
        }

        public function __construct($scope)
        {
            $this->scope = $scope;
        }

        protected function bin()
        {
            if (defined(static::name())) {
                return constant(static::name());
            }

            return $this->scope == self::SYSTEM ? bin(static::system()) : static::local();
        }

        protected function wrap($command)
        {
            return array(self::uniqueKey() => $command);
        }

    }
}


namespace modules
{
    class Drush extends \Mudi\Module
    {

        private static $siteName = 'mudi-drupal-install';


        public static function name()
        {
            return 'drush';
        }

        public static function system()
        {
            return 'drush';
        }

        public static function local()
        {
            return './drush';
        }

        public static function defaultScope()
        {
            return self::SYSTEM;
        }

        public static function defaultAction()
        {
            return 'make';
        }

        private static function generateSiteName()
        {
            self::$siteName = 'install' . mt_rand();
        }

        public function make($makefile = 'drupal.make')
        {
            $before = function () {
                if (is_dir('sites/default')) {
                    chmod('sites/default', 0755);
                }
            };

            $command = "{$this->bin()} make $makefile .";

            return $this->wrap(new \Mudi\Task($before, $command, null));
        }

        public function install($profile)
        {
            self::generateSiteName();

            $site = self::$siteName;
            $path = "sites/$site";

            if (is_readable('/dev/shm')) {
                $database = 'sqlite:/dev/shm/' . $site . '.sqlite';
            } else {
                $database = 'sqlite:' . $path . '/files/database.sqlite';
            }

            $before = function () use ($path) {
                if (is_dir($path)) {
                    throw new \Mudi\Exception(sprintf('Drupal installation directory %s already exists.', $path));
                }

                mkdir($path);
                touch("$path/settings.php");
            };

            $arguments = array(
                $this->bin(),
                'site-install',
                $profile,
                'install_configure_form.update_status_module=\'array(FALSE, FALSE)\'',
                '--db-url="' . $database . '"',
                '--account-name=root',
                '--account-pass=toor',
                '--sites-subdir=' . $site,
                '-y',
            );

            $after = function () use ($path) {
                chmod($path, 0755);

                if (is_dir('sites/default/modules')) {
                    symlink('../default/modules', "$path/modules");
                }

                if (is_dir('sites/default/themes')) {
                    symlink('../default/themes', "$path/themes");
                }
            };

            return $this->wrap(new \Mudi\Task($before, implode(' ', $arguments), $after));
        }

        public function clear()
        {
            $site = self::$siteName;
            $path = "sites/$site";

            $after = function () use ($path) {
                if (!is_dir($path)) {
                    return;
                }

                chmod($path, 0755);

                foreach(new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path, \FilesystemIterator::SKIP_DOTS), \RecursiveIteratorIterator::CHILD_FIRST) as $i) {
                    $i->isDir() && !$i->isLink() ? rmdir($i->getPathname()) : unlink($i->getPathname());
                }

                rmdir($path);
            };

            return $this->wrap(new \Mudi\Task(null, '', $after));
        }

        public function enable()
        {
            $site = self::$siteName;
            $projects = implode(' ', func_get_args());

            return $this->wrap("{$this->bin()} pm-enable $projects --uri=http://$site/ -y");
        }

        public function exec()
        {
            $site = self::$siteName;
            $command = implode(' ', func_get_args());

            return $this->wrap("{$this->bin()} $command --uri=http://$site/ -y");
        }

    }


    class Npm extends \Mudi\Module
    {

        public static function name()
        {
            return 'npm';
        }

        public static function system()
        {
            return 'npm';
        }

        public static function defaultScope()
        {
            return self::SYSTEM;
        }

        public static function defaultAction()
        {
            return 'install';
        }

        public function install()
        {
            return $this->wrap("{$this->bin()} install");
        }

    }


    class Bower extends \Mudi\Module
    {

        public static function name()
        {
            return 'bower';
        }

        public static function system()
        {
            return 'bower';
        }

        public static function local()
        {
            return './node_modules/.bin/bower';
        }

        public static function defaultScope()
        {
            return self::SYSTEM;
        }

        public static function defaultAction()
        {
            return 'install';
        }

        public function install()
        {
            return $this->wrap("{$this->bin()} install");
        }

    }


    class Bundle extends \Mudi\Module
    {

        public static function name()
        {
            return 'bundle';
        }

        public static function system()
        {
            return 'bundle';
        }

        public static function defaultScope()
        {
            return self::SYSTEM;
        }

        public static function defaultAction()
        {
            return 'install';
        }

        public function install()
        {
            return $this->wrap("{$this->bin()} install --standalone --binstubs");
        }

    }


    class Compass extends \Mudi\Module
    {

        public static function name()
        {
            return 'compass';
        }

        public static function system()
        {
            return 'compass';
        }

        public static function local()
        {
            return './bin/compass';
        }

        public static function defaultScope()
        {
            return self::LOCAL;
        }

        public static function defaultAction()
        {
            return 'compile';
        }

        public function compile()
        {
            return $this->wrap("{$this->bin()} compile");
        }

    }


    class Composer extends \Mudi\Module
    {

        public static function name()
        {
            return 'composer';
        }

        public static function system()
        {
            return 'composer';
        }

        public static function local()
        {
            return 'php composer.phar';
        }

        public static function defaultScope()
        {
            return self::LOCAL;
        }

        public static function defaultAction()
        {
            return 'install';
        }

        public function install()
        {
            $scope = $this->scope;

            $before = function () use ($scope) {
                if ($scope == \Mudi\Module::LOCAL) {
                    if (!is_file('composer.phar')) {
                        echo "\n# composer.phar is not found. The latest version will be downloaded...\n";

                        $composer = file_get_contents('https://getcomposer.org/composer.phar');
                        file_put_contents('composer.phar', $composer);
                    }
                }
            };

            $command = "{$this->bin()} install";

            return $this->wrap(new \Mudi\Task($before, $command, null));
        }

    }
}


namespace {
    function bin($executable) {
        $notFound = '%s is necessary to install dependencies but is not found.';

        if ($bin = `which $executable 2>/dev/null`) {
            return trim($bin);
        } else {
            throw new \Mudi\Exception(sprintf($notFound, $executable));
        }
    }


    function create_module($className) {
        if (!class_exists($className) || !is_subclass_of($className, '\Mudi\Module')) {
            return false;
        }

        $name = $className::name();
        $default = $className::defaultScope();

        if (!in_array($default, array(\Mudi\Module::SYSTEM, \Mudi\Module::LOCAL))) {
            return false;
        }

        $classReflection = new ReflectionClass($className);
        $publicMethods = $classReflection->getMethods(ReflectionMethod::IS_PUBLIC);

        foreach ($publicMethods as $method) {
            $action = $method->name;
            if ($method->isStatic() || $action == '__construct') {
                continue;
            }

            eval("namespace $name {
                function $action() {
                    return call_user_func_array('\\{$default}\\{$name}\\{$action}', func_get_args());
                }
            }");

            if ($className::system()) {
                eval("namespace system\\{$name} {
                    function $action() {
                        \$handler = \\system\\{$name}\\bin();

                        return call_user_func_array(array(\$handler, '$action'), func_get_args());
                    }
                }");
            }

            if ($className::local()) {
                eval("namespace local\\{$name} {
                    function $action() {
                        \$handler = \\local\\{$name}\\bin();

                        return call_user_func_array(array(\$handler, '$action'), func_get_args());
                    }
                }");
            }
        }

        if ($action = $className::defaultAction()) {
            eval("namespace {
                function $name() {
                    return call_user_func_array('\\{$default}\\{$name}\\{$action}', func_get_args());
                }
            }");

            if ($className::system()) {
                eval("namespace system {
                    function $name() {
                        return call_user_func_array('\system\\{$name}\\{$action}', func_get_args());
                    }
                }");
            }

            if ($className::local()) {
                eval("namespace local {
                    function $name() {
                        return call_user_func_array('\local\\{$name}\\{$action}', func_get_args());
                    }
                }");
            }
        }

        if ($className::system()) {
            eval("namespace system\\{$name} {
                function bin() {
                    return new \\{$className}('system');
                }
            }");
        }

        if ($className::local()) {
            eval("namespace local\\{$name} {
                function bin() {
                    return new \\{$className}('local');
                }
            }");
        }
    }
}


namespace {
    set_exception_handler(function ($e) {
        echo sprintf("\n%s\n", $e->getMessage());
        exit(1);
    });

    $app = new Mudi\Environment();

    if (is_file(__DIR__ . '/' . $_SERVER['PHP_SELF'])) {
        $__source = file_get_contents(__DIR__ . '/' . $_SERVER['PHP_SELF']);
        $__variables = array();
        preg_match_all('/\$([a-z][a-z0-9_]*)/', $__source, $__variables, PREG_SET_ORDER);

        foreach ($__variables as $__variable) {
            if (!isset($GLOBALS[$__variable[1]])) {
                $GLOBALS[$__variable[1]] = bin($__variable[1]);
            }
        }

        foreach (get_declared_classes() as $__class) {
            if (is_subclass_of($__class, '\Mudi\Module')) {
                if ($__name = $__class::name()) {
                    if (preg_match('/\b' . $__name . '\b/', $__source)) {
                        create_module($__class);
                    }
                }
            }
        }

        unset($__source, $__variable, $__class, $__name);
    }
}
