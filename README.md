MUDI (Multi Dependencies Installer)
===================================

---

Install all project dependencies with a single install script.

MUDI is not a package manager, and it cannot be used to download `composer` or
`npm` packages.

The goal of this project is to facilitate initial installation of modern web
applications that use `npm`, `bower`, `composer`, `Ruby gems`, etc. to install
dependencies into multiple directories.

With MUDI, all used package managers can be listed in a single `install` script
located at the top directory of the project. When it is run, this script will
simply execute an appropriate install commands in each configured directory.

---

Example usage
-------------

In an example project with the following directory structure:

```
/
+---app/
+---src/
+---resource/
|   +---sass/
|   |   +---Gemfile
|   |   +---config.rb
|   |
|   +---templates/
|
+---public/
|   +---bower.json
|   +---package.json
|
+---composer.json
+---install

```

...the `install` script may have the following contents:

```php

// Install composer at the root of the project.
$app['/'] = composer();

// Install bower and npm packages in the public directory.
$app['/public'] = bower() + npm();

// Install Ruby gems in sass directory and compile stylesheets using freshly
// installed compass.
$app['/resources/sass'] = bundle() + local\compass();

```

---

Quick start
===========

MUDI is designed to be self-contained – it has dependency only on itself.

First, create an `install` script at the root directory of the project using
the template provided below, and add `.dependencies.mudi.php` to the
`.gitignore` file. Make the script executable.

To run it, navigate to the top directory of your project and run the following code in the command line:

```bash

./install

# or

php install

```

MUDI should be able to download itself and tell you if your system is missing
some of the package managers used in the project. If there are no problems,
package managers will start downloading packages and you will see the console
output and interact with them normally.

It should be safe to run the `install` script multiple times at any moment,
though the exact result of each execution may differ depending on the behavior
of the used package managers.

---

Install script template
=======================

Copy the following code to a new `install` file and make any necessary
modifications. It is locked to always use the same version of MUDI (which was
the latest when you created the `install` file). If you want to use a newer
version of MUDI, you need to manually replace the `install` script contents
with a new template.

```php
#!/usr/bin/env php
<?php

$mudi = __DIR__ . '/.dependencies.mudi.php';
is_file($mudi) ? require $mudi : !strcmp(hash('sha256', $__ = file_get_contents
    ('https://bitbucket.org/20th/mudi-installer/raw/v1.1.1/dependencies.php')),
     '26bed3e4c4d5ec28e46482cd0af54d34789267f11e0ade40a63b7832eaabd5cf')
&& file_put_contents($mudi, $__) && require $mudi;


// Put your installation commands starting from this line.
// Commands executor is available as $app variable.

$app['/'] = composer();

```
